---
layout: default
top: 11
startposition: 2000
fee: 7.90
js: 
- assets/js/json.js
- assets/js/topselektor.js
---
<script>
    var TOP={{ page.top }};
    var STARTPOSITION={{ page.startposition }};
    var FEE={{ page.fee }};
</script>

# Strategy-Investor.de - Topselektor - Performance
{: class="fixed" }

{% include menu.md aktiv="T11" %}

Diese Topliste enthält für jeden US-Sektor jeweils den aus meiner Sicht kurz- bis mittelfristig aussichtsreichsten Wert. Mit einer kalkulierten maximalen Positionsgröße von <input type="text" id="startposition" value="{{ page.startposition }}" onchange="calcPerformance(); getTrades();">€ unter Einberechnung einer Transaktionsgebühr von <input type="text" id="fee" value="{{ page.fee | decimals }}" onchange="calcPerformance(); getTrades();">€ ( bei flatex üblich ) wird neben der theoretischen wöchentlichen Kursperformance eine realitätsnahe geringer ausfallende Wertperformance errechnet. Ziel ist es eine überdurchschnittliche Performance erreichen zu können.
{: style="text-align: justify;" }

<u>Keine</u> Kaufempfehlungen!<br>Jeder Anleger trägt das Risiko <u>seiner</u> Entscheidung!

<button type="button" id="toggle_Kursperformance" class="btn selected" onclick="toggleView(this)">Kursperformance</button><button type="button" id="toggle_Positionsperformance" class="btn" onclick="toggleView(this)">Positionsperformance</button>
<button type="button" id="toggle_Trades" class="btn" onclick="toggleView(this)">Trades</button>

<div id="performance" class=""></div>

<table id="top10" class="overview top">
    <thead>
        <tr>
            <th>Woche</th><th>P1</th><th>P2</th><th>P3</th><th>P4</th><th>P5</th><th>P6</th><th>P7</th><th>P8</th><th>P9</th><th>P10</th><th>P11</th><th>W%</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<table id="trades" class="overview trades">
<thead>
    <tr>
        <th>Anlage</th><th>Stk</th><th colspan="2">Einstieg</th><th colspan="2">Ausstieg</th><th>Perf</th>
    </tr>
</thead>
<tbody>
</tbody>
</table>