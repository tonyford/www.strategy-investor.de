---
layout: default
js: 
- assets/js/json.js
- assets/js/matrix.js
---
<style>
em, strong { color: aliceblue; }
</style>
<script>
</script>
# Strategy-Investor.de - Top 1 - Performance
{: class="fixed" }

{% include menu.md aktiv="S" %}

<u>Keine</u> Kaufempfehlungen!<br>Jeder Anleger trägt das Risiko <u>seiner</u> Entscheidung!

Auf dieser Seite erfolgt eine Betrachtung der Performance nach verschiedenen Indikationen als wöchentliche Kursperformance sowie der Performance seit Beginn in Form von normalisierten Indizes.  

<button type="button" id="toggle_Kursperformance" class="btn selected" onclick="toggleView(this)">Kursperformance</button><button type="button" id="toggle_Positionsperformance" class="btn" onclick="toggleView(this)">Indizes</button>

<div id="performance" class=""></div>

<table id="matrix" class="overview top">
    <thead>
        <tr>
            <th>Woche</th>
            <th title="Entwicklung des DAX als Referenzwert">DAX</th>
            <th title="Entwicklung aller Werte auf der Strategy-Investor - Watchlist">WL</th>
            <th title="Entwicklung aller Werte auf der Strategy-Investor - Watchlist, jedoch gewichtet nach Rating.">WL<br><sub>gewichtet</sub></th>
            <th title="Entwicklung aller Werte mit Strategy-Investor - Rating 0 bis 1.xx">W1</th>
            <th title="Entwicklung aller Werte mit Strategy-Investor - Rating 2.xx">W2</th>
            <th title="Entwicklung aller Werte mit Strategy-Investor - Rating 3.xx">W3</th>
            <th title="Entwicklung aller Werte mit Strategy-Investor - Rating 4.xx">W4</th>
            <th title="Entwicklung aller Werte mit Strategy-Investor - Rating 5.xx+">W5</th>
            <th title="Entwicklung aller als Highflyer gekennzeichneten Werte, d.h. Werte welche kürzlich ein 52W erreicht haben und max. 10% unterhalb notieren">HF</th>
            <th title="Entwicklung aller Werte, welche zuletzt kein 52W Hoch erreicht haben oder mind. 10% entfernt von einem 52W Hoch notieren">noHF</th>
            <th title="Entwicklung aller Werte, welche in den letzten 30 Tagen eine positive Performance erreichten.">1M</th>
            <th title="Entwicklung aller Werte, welche Highflyer + TradingView Kauf erreicht haben">HF+1M</th>
            <th title="Entwicklung aller Werte, welche charttechnisch nahe am gleitenden Durchschnitt (Moving Average) standen ( Aufwärtstrend ) oder weit vom MA entfernt waren ( Abwärtstrend )">MAΔ</th>
            <th title="Entwicklung aller Werte, welche mind. ein Strategy-Investor - Rating von 5 + Highflyer + 1M positiv erreicht haben">W5+HF+1M</th>
            <th title="Entwicklung des Strategy-Investor - Top1 - Musterdepots">Top1</th>
            <th title="Entwicklung des Strategy-Investor - Top5 - Musterdepots">Top5</th>
            <th title="Entwicklung des Strategy-Investor - Top10 - Musterdepots">Top10</th>
        </tr> 
    </thead>
    <tbody>
    </tbody>
</table>
