---
layout: default
js: 
    - assets/js/json.js
    - assets/js/script.js
---

# Strategy-Investor.de - Watchlist
{: class="fixed" }

<u>Keine</u> Kaufempfehlungen!<br>Jeder Anleger trägt das Risiko <u>seiner</u> Entscheidung!

{% include menu.md aktiv="" %}

<h3>Aktien - Watchlist</h3>
<table id="aktien" class="overview">
<thead>
<tr>
    <th>Watchlist<br><small>Dividende ExTag</small></th>
    <th>Ranking<br>Score</th>
    <th>Kurs</th>
    <th>Perf<br><small>watchlist</small></th>
    <th>Unternehmen<br>Tags</th>
    <th>UΔ<sub>27</sub></th>
    <th>UΔ<sub>26</sub></th>
    <th>UΔ<sub>25</sub></th>
    <th title="Fair Value Offset">FVΔ</th>
    <th>Kommentar</th>
</tr>
</thead>
<tbody>
</tbody>
</table>

<hr>
<h3>US - Sektor - Selektion</h3>
<table id="selektoren" class="overview">
<thead>
<tr>
    <th>Watchlist</th>
    <th>&nbsp;</th>
    <th>Kurs</th>
    <th>Perf<br><small>watchlist</small></th>
    <th>Aktie<br>Sektor</th>
    <th>&nbsp;</th>
    <th>Info</th>
</tr>
</thead>
<tbody>
</tbody>
</table>

<hr>
<h3>ETF - Watchlist</h3>
<table id="etf" class="overview">
<thead>
<tr>
    <th>Watchlist</th>
    <th>Ranking<br>Score</th>
    <th>Kurs</th>
    <th>Perf<br><small>watchlist</small></th>
    <th>ETF<br>Tags</th>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <th>Kommentar</th>
</tr>
</thead>
<tbody>
</tbody>
</table>

<table class="overview">
<tr>
    <th></th>
</tr>
</table>

#### Fair Value Offset ( FVΔ )

Stark vereinfacht ist für mich ein Wert fair bewertet, wenn das Kurs-Gewinn-Verhältnis dem doppelten erwarteten Umsatzwachstum der Folgejahre entspricht.

Der Fair-Value-Offset, ob unter FVΔ in der Tabelle zu finden, zeigt die Abweichung zu diesem Wert. Als Berechnungsparameter nutze ich das durchschnittliche Umsatzwachstum von 2024-2026 sowie das Kurs-Gewinn-Verhältnis 2025. 

Um auch junge noch verlustreiche Wachstumsunternehmen bewerten zu können, wird bei Verlust in 2025 eine Gewinn-Marge von 10% bezogen auf den Umsatz, angenommen und ein fiktiver/potenzieller Gewinn errechnet. In der Tabelle sind die Offset-Werte mit einem * gekennzeichnet. 

Je höher der Wert, desto größer das theoretische Kurs(aufhol)potenzial. Für wachstumsschwache Werte liefert diese vereinfachte Berechnung weniger zufriedenstellenden Ergebnisse. Auch berücksichtigt diese Berechnung keine marktspezifischen branchenspezifischen Faktoren, welche zu unterschiedlichen Bewertungsmaßstäben führen. Deshalb dient dieser Fair-Value-Offset primär zur Orientierung und des Vergleichs.  

