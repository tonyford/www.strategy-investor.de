---
--- 

var DAX = {{ site.data.DAX | jsonify }};
var W = {{ site.data.watchlist | jsonify }};
//var K = {{ site.data.kurs | jsonify }};
var K = content_load('kurse.json');
var R = {};
var RR = {};
const RECT_SIZE=10;

var _e = (el) => ( document.createElement(el) );

var Aktien = [];
var lastAktien = [];

// Musterdepot per Datum
var WPD={};


Array.prototype.diff = function(arr2) { 
    return this.filter(x => !arr2.includes(x)); 
  }

$(document).ready(
    function(){

        Object.entries( W.etf ).forEach(
            function(v,i){
                if( v[1].rating ){
                    Object.entries(v[1].rating).forEach(
                        function(w,j){
                            if( R[w[0]] == undefined ) R[w[0]] = {}
                            R[w[0]][v[0]] = w[1] ;
                        }
                    );
                }
               
            }
        );

        Object.entries(R).forEach( 
            function(v,i){
                if( RR[v[0]] == undefined ) RR[v[0]] = [];
                Object.entries(v[1]).forEach(
                    function(w,j){
                        RR[v[0]].push( [ w[0], w[1], w[1][3]+ w[1][4] + w[1][5] ] );
                        
                    }    
                );
            }
        );
        Object.entries(RR).forEach(
            function(v){
                RR[v[0]].sort((a,b) => ( b[2] - a[2] ));
                if( W.topetf[v[0]] != undefined ){
                    var toplist=W.topetf[v[0]].map((c) => ( RR[v[0]].filter((d) => (d[0] == c )) ) ).map((c) => (c[0]));
                    RR[v[0]] = toplist.concat( RR[v[0]].diff( toplist ));
                }
            }    
        );
        
        calcPerformance();
        getTrades();
    }
);

function calcPerformance(){


    var DPerf=1;
    var DAXstart=1;
    var DAXende=1; 
    var lastDat=undefined;
    $('table.top tbody').html('');

    Object.entries(RR).sort().forEach(
        function(w,j,array){
            var WP = [];

            // Musterdepot fuer Datum erstellen
            WPD[w[0]] = { 'cash' : j == 0 ? $('#startposition').val() * TOP : WPD[lastDat].cash };
            var Wpd = WPD[w[0]];

            lastAktien = Aktien;
            Aktien=[];

            
            var dat = w[0].slice(1,5) + '-' + w[0].slice(5,7) + '-' + w[0].slice(7,9);
            var Dat = w[0];

            if( Dat.slice(1,5) != lastDat?.slice(1,5) && lastDat != undefined ){
                var tr = _e('tr');
                var td = _e('td');
                td.classList = 'dat'
                td.append(_e('br'));
                var span = _e('span');
                span.innerHTML='<u>'+lastDat.slice(1,5)+'</u>';
                span.style='color: goldenrod; font-size:1.8em;';
                td.append(span);
                td.append(_e('br'));

                var span = _e('span');
                span.classList = 'toggle_Positionsperformance ' + ( WPD[lastDat].total_perf >= 1 ? 'pos' : 'neg' );
                span.title = 'Kalkulierte Gesamtperformance aller offenen und geschlossenen Positionen';
                span.innerHTML = ( WPD[lastDat].total_perf >= 1 ? '+' : '' ) + (100*(WPD[lastDat].total_perf-1)).toFixed(2) + '%<br>';
                td.append(span);

                var span = _e('span');
                span.classList = 'kursperformance toggle_Kursperformance ' + ( DPerf >= 1 ? 'pos' : 'neg' );
                span.title = 'Multiplizierte mittlere Wochen-Kursperformance';
                span.innerHTML = ( DPerf >= 1 ? '+' : '' ) + (100*(DPerf-1)).toFixed(2)+'%<br>';
                td.append(span);
                
                var DAXyPerf = 100* ( DAXende/DAXstart-1 );
                var small = _e('small');
                small.classList = 'toggle_Kursperformance ' +( DAXyPerf >= 1 ? 'pos' : 'neg');
                small.innerHTML = 'DAX ' +(DAXyPerf >= 0 ? '+' : '')+ (DAXyPerf).toFixed(2);
                td.append(small);

                var small = _e('small');
                small.classList = 'toggle_Positionsperformance ' +( DAXyPerf >= 1 ? 'pos' : 'neg');
                small.innerHTML = 'DAX ' +(DAXyPerf >= 0 ? '+' : '')+ (DAXyPerf).toFixed(2);
                td.append(small);
                
                td.colSpan=14;
                tr.append(td);
                $('table.top tbody').prepend(tr);

                //WPD[Dat] = { 'cash' : $('#startposition').val() * TOP, 'total_perf' : 1 };
                var DAXkurs = K.DAX && j == array.length - 1 ? K.DAX : DAX[w[0]][1];
                DAXstart = DAXkurs;
                DAXstart_dat = dat;
                DPerf = 1;
                //einstieg = true;
                //console.log( JSON.parse(JSON.stringify( WPD[lastDat])) );
                //WPD[lastDat]={ 'cash' : $('#startposition').val() * TOP, 'total_perf' : 1 };
                WPD[lastDat]={};
                WPD[Dat].cash = $('#startposition').val() * TOP;
            }

            if( j == 0 ){
                DAXstart = DAX[w[0]][0];
                DAXstart_dat = dat;
            } 
            var tr = _e('tr');
            var td = _e('td');
            td.classList = 'dat'
            td.innerHTML=dat;
            tr.append(td);

            var TD = w[1].filter((a,b) => (b<TOP)).map(
                function(v,i){
                    var aktie = W.etf[v[0]];
                    var aktie_id = v[0];
                    
                    Aktien.push(aktie_id);

                    // Aktie in Musterdepot schreiben
                    WPD[w[0]][v[0]]={} 
                    var wpd = WPD[w[0]][v[0]];
                    var last_wpd = lastDat != undefined ? WPD[lastDat][v[0]] : undefined;

                    //var einstieg = lastAktien.indexOf(aktie_id) == -1;
                    var einstieg = lastDat != undefined ? WPD[lastDat][v[0]] == undefined : true;

                    wpd['start'] = v[1][0];
                    var kurs = K[aktie.isin] ? K[aktie.isin] : v[1][1];
                    var waehrung_kurs = Object.values(aktie.rating)[0][2];
                    var waehrung_dataimport = aktie.dataimport ? aktie.dataimport : waehrung_kurs;
                    var waehrung_umr = ( waehrung_kurs != waehrung_dataimport && K[aktie.isin] ) ? W.waehrungen[ waehrung_kurs+aktie.dataimport ][0] : 1;
                    kurs /= waehrung_umr;
                    wpd['ende'] = j === array.length - 1 ? kurs : v[1][1];
                    wpd['einstieg'] = einstieg ? v[1][0] : last_wpd.einstieg;
                    wpd['einstieg_datum'] = einstieg ? new Date(w[0].slice(1,5) + '-' + w[0].slice(5,7) + '-' + w[0].slice(7,9) + 'UTC' ) : last_wpd.einstieg_datum;
                    wpd['woche_im_depot'] = einstieg ? 1 : last_wpd.woche_im_depot + 1; 
                    wpd['w_perf'] = wpd.ende / wpd.start;
                    wpd['e_perf'] = wpd.ende / wpd.einstieg;
                    wpd['stk'] = einstieg ? parseInt( ($('#startposition').val()*1-$('#fee').val()*1) / wpd.einstieg ) : last_wpd.stk;
                    wpd['e_wert'] = einstieg ? wpd.einstieg * wpd.stk : last_wpd.e_wert;
                    wpd['w_wert'] = wpd.stk * wpd.ende;
                    wpd['divi_stk'] = $.isNumeric(v[1][8]) ? v[1][8] : 0;
                    wpd['dividende'] = wpd.divi_stk * wpd.stk;
                    Wpd.cash -= einstieg ? wpd.e_wert + $('#fee').val()*1 : 0; 
                    wpd['total'] = wpd.w_wert + wpd.dividende - $('#fee').val()*1;
                    wpd['total_perf_incl_fee'] = wpd.total / wpd.e_wert;
                    //wpd['ausstieg_datum'] = new Date(w[0].slice(1,5) + '-' + w[0].slice(6,7) + '-' + w[0].slice(8,9) );

                    var td = _e('td');
                    td.classList='position eng';

                    // Ranking & Score
                    var spann = _e('span');
                    spann.classList = 'score';
                    WP.push( ( j === array.length - 1 ? kurs : v[1][1] ) / v[1][0]);

                    var dividende = v[1][8] ? v[1][8] : 0;
                    var perf = 100*(( ( ( j === array.length - 1 ? kurs : v[1][1] ) + dividende ) / v[1][0])-1);

                    var span = _e('span');
                    span.classList = 'compact '+ ( einstieg ? 'einstieg' : '' );
                    span.innerHTML = aktie.anlage.slice(0,14)+'<sup>' + wpd.woche_im_depot + '</sup><br>';
                    spann.append(span);

                    
                    var span = _e('span');
                    span.classList = 'toggle_Positionsperformance perff ' + ( wpd.total_perf_incl_fee >= 1 ? 'pos' : 'neg' );
                    span.title = 'Kalkulierte Positionsperformance seit Einstieg unter Einberechnung der Transaktionsgebühren';
                    var spa = _e('span');
                    spa.classList = 'runter';
                    spa.innerHTML = ( wpd.total_perf_incl_fee >= 1 ? '+' : '' ) + (100*(wpd.total_perf_incl_fee-1)).toFixed(2) +'%';
                    span.append(spa);
                    span.append(_e('br'));
                    var small = _e('small');
                    small.innerHTML = wpd.einstieg.toFixed(3) + ' - ' + ( j === array.length - 1 ? kurs : v[1][1] ).toFixed(3);
                    small.title = 'Positionsperformance\n' + wpd.einstieg.toFixed(3) + ' ' + v[1][2] + ' - ' + ( j === array.length - 1 ? kurs : v[1][1] ).toFixed(3) + ' ' + v[1][2];
                    span.append(small);
                    span.append(_e('br'));

                    spann.append(span);

                    var span = _e('span');
                    span.classList = 'kursperformance toggle_Kursperformance '+ ( perf >= 0 ? 'pos' : 'neg' );
                    span.title = 'Kursperformance\n' + v[1][0].toFixed(3) + ' ' + v[1][2] + ' - ' + ( j === array.length - 1 ? kurs : v[1][1] ).toFixed(3) + ' ' + v[1][2];
                    //span.innerHTML = ((perf >= 0) ? '+' : '') +perf.toFixed(2)+'%' + (( dividende > 0 ? '<sub>Divi</sub>' : '' ))+ '<br>';
                    var spa = _e('span');
                    spa.classList = 'runter';
                    spa.title = span.title; 
                    spa.innerHTML = ((perf >= 0) ? '+' : '') +perf.toFixed(2)+'%' + (( dividende > 0 ? '<sub>Divi</sub>' : '' ));
                    span.append(spa);
                    span.append(_e('br'));
                    var small = _e('small');
                    small.innerHTML = v[1][0].toFixed(3) + ' - ' + ( j === array.length - 1 ? kurs : v[1][1] ).toFixed(3);
                    span.append(small);
                    span.append(_e('br'));
                    
                    spann.append(span);

                    var small = _e('small');
                    small.classList = 'ranking toggle_Kursperformance';
                    small.title = 'Rang';
                    small.innerHTML = (i+1);
                    spann.append(small);

                    spann.append(' ');
                    
                    var span = _e('span');
                    span.classList = 'toggle_Kursperformance';
                    span.title = 'Meine Wertung';
                    span.innerHTML = v[2].toFixed(2)+'<br>';
                    spann.append(span);

                    var span = _e('span');
                    span.classList = 'stk toggle_Positionsperformance';
                    span.title = 'Kalkulierte Stückzahl';
                    span.innerHTML =  wpd.stk + ' stk<br>';
                    spann.append(span);


                    td.append(spann);

                    // Highflyer 
                    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                    svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                    svg.setAttribute('width', RECT_SIZE);
                    svg.setAttribute('height', RECT_SIZE);
                    
                    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                    rect.setAttribute('fill', v[1][3] == 1 ? 'darkseagreen' : ( v[1][3] == 0.5 ? 'darkslategrey' : 'black' ) );
                    rect.setAttribute('width', RECT_SIZE);
                    rect.setAttribute('height', RECT_SIZE);
                    rect.innerHTML='<title>Abstand zum 52W Hoch?</title>';
    
                    svg.appendChild(rect);
                    td.append(svg);
                    td.append(' ');

                    // TV rating
                    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                    svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                    svg.setAttribute('width', RECT_SIZE);
                    svg.setAttribute('height', RECT_SIZE);

                    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                    rect.setAttribute('fill', v[1][4] == 1 ? 'darkseagreen' : ( v[1][4] == 0.5 ? 'darkslategrey' : 'black' ) );
                    rect.setAttribute('width', RECT_SIZE);
                    rect.setAttribute('height', RECT_SIZE);
                    rect.innerHTML='<title>Monatsperformance?</title>';
                    svg.appendChild(rect);
                    td.append(svg);
                    td.append(' ');

                    // Chart
                    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                    svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                    svg.setAttribute('width', RECT_SIZE);
                    svg.setAttribute('height', RECT_SIZE);

                    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                    rect.setAttribute('fill', v[1][5] == 1 ? 'darkseagreen' : ( v[1][5] == 0.5 ? 'darkslategrey' : 'black' ) );
                    rect.setAttribute('width', RECT_SIZE);
                    rect.setAttribute('height', RECT_SIZE);
                    rect.innerHTML='<title>Abstand zum gleitenden Durchschnitt (50/200)?</title>';
                    svg.appendChild(rect);
                    td.append(svg);
                    td.append(' ');

                    // Fundamental
                    /*
                    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                    svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                    svg.setAttribute('width', RECT_SIZE);
                    svg.setAttribute('height', RECT_SIZE);

                    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                    rect.setAttribute('fill', v[1][6] == 1 ? 'darkseagreen' : ( v[1][6] == 0.5 ? 'darkslategrey' : 'black' ) );
                    rect.setAttribute('width', RECT_SIZE);
                    rect.setAttribute('height', RECT_SIZE);
                    rect.innerHTML='<title>Marketscreener?</title>';
                    svg.appendChild(rect);
                    td.append(svg);
                    */

                    return td;
                }
            );

            TD.forEach((td) => (tr.append(td)));
            var WPerf = WP.reduce((a,b) => (a+b)) / WP.length;
            var DAXkurs = K.DAX && j == array.length - 1 ? K.DAX : DAX[w[0]][1];
            var DAXp = DAXkurs / DAX[w[0]][0];
            var DAXpp = DAXkurs / DAXstart;
            DPerf = DPerf * WPerf;
            DAXende = DAXkurs;
            DAXende_dat = dat;
            var td = _e('td');

            if( j > 0 ){
                var WerteW = Object.keys(Wpd);
                var WerteVW = Object.keys(WPD[lastDat] );
                var WerteDiff = WerteVW.diff(WerteW);
                WerteDiff.forEach(
                    function(z){
                        Wpd.cash += WPD[lastDat][z].total ? WPD[lastDat][z].total : 0;
                        WPD[lastDat][z].ausstieg_datum = new Date(w[0].slice(1,5) + '-' + w[0].slice(5,7) + '-' + w[0].slice(7,9) + 'UTC' );                      
                    }
                );
            }

            var total_perf = Object.values(Wpd).map((a) => (a.total ? a.total : 0 )).reduce((a,b) => (a+b)) + Wpd.cash;
            total_perf = total_perf / ( $('#startposition').val() * TOP );
            Wpd['total_perf'] = total_perf;
            td.classList = 'perform ';
            td.classList += WPerf >= 1 ? 'pos' : 'neg';

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance perff ' + (total_perf >= 1 ? 'pos' : 'neg' );
            span.innerHTML = (total_perf >= 1 ? '+' : '' ) + (100*(total_perf-1)).toFixed(2) + '%<br>';
            td.append(span);

            var span = _e('span');
            span.classList = 'kursperformance toggle_Kursperformance';
            span.title = 'Mittlere Kursperformance';
            span.innerHTML = ( WPerf >= 1 ? '+' : '' ) + (( WPerf - 1 ) * 100 ).toFixed(2) + '<sup>%</sup><br>';
            td.append(span);

            var small = _e('small');
            small.classList = 'toggle_Kursperformance ' +( DAXp >= 1 ? 'pos' : 'neg');
            small.innerHTML = 'DAX ' +(DAXp >= 1 ? '+' : '')+ (100*(DAXp-1)).toFixed(2);
            td.append(small);

            var small = _e('small');
            small.classList = 'toggle_Positionsperformance ' +( DAXpp >= 1 ? 'pos' : 'neg');
            small.innerHTML = 'DAX ' +(DAXpp >= 1 ? '+' : '')+ (100*(DAXpp-1)).toFixed(2);
            td.append(small);

            tr.append(td);
                
            $('table.top tbody').prepend(tr);
            lastDat=w[0];
        }
    );
    DPerf_all = DAXende / DAXstart;

    $('#performance').html('');

    var span = _e('span');
    span.innerHTML='<u>'+lastDat.slice(1,5)+'</u><br>';
    span.style='color: goldenrod; font-size:1.8em;';
    $('#performance').append(span);

    var span = _e('span');
    span.classList = 'toggle_Positionsperformance ' + ( WPD[lastDat].total_perf >= 1 ? 'pos' : 'neg' );
    span.title = 'Kalkulierte Gesamtperformance aller offenen und geschlossenen Positionen';
    span.innerHTML = ( WPD[lastDat].total_perf >= 1 ? '+' : '' ) + (100*(WPD[lastDat].total_perf-1)).toFixed(2) + '%<br>';
    $('#performance').append(span);

    var span = _e('span');
    span.classList = 'kursperformance toggle_Kursperformance';
    span.title = 'Multiplizierte mittlere Wochen-Kursperformance';
    span.innerHTML = ( DPerf >= 1 ? '+' : '' ) + (100*(DPerf-1)).toFixed(2)+'%<br>';
    $('#performance').append(span);
    
    var small = _e('small');
    small.classList = ( DPerf_all >= 1 ? 'pos' : 'neg' );
    small.title = 'DAX-Performance\n' + DAXstart_dat + ': ' + DAXstart.toFixed(0) + ' Pkt\n' + DAXende_dat + ': ' + DAXende.toFixed(0) + ' Pkt';
    small.innerHTML = 'DAX '+ (DPerf_all >= 1 ? '+' : '') + (100*(DPerf_all-1)).toFixed(2) +'%';
    $('#performance').append(small);

    $('#performance').addClass( DPerf >= 1 ? 'pos' : 'neg' );
    
    $('.btn.selected').click();

}

function getTrades(){

    var lastDat=undefined;

    $('table#trades tbody').html('');

    Object.entries(WPD).forEach(
        function(v,i){

            if( lastDat != undefined ){
                var Ex = Object.keys( WPD[lastDat] ).diff("total_perf").diff("cash");
                var Ne = Object.keys( v[1] ).diff("total_perf").diff("cash");
                var Tr = Ex.diff(Ne);

                Tr.forEach(
                    function(w,j){
                        var wpd = WPD[lastDat][w];
                        var aktie_id = w;
                        var aktie = W.etf[aktie_id];

                        var tr = _e('tr');

                        var td = _e('td');
                        td.innerHTML = aktie.anlage;
                        tr.append(td);

                        var td = _e('td');
                        td.innerHTML = wpd.stk;
                        tr.append(td);

                        var td = _e('td');
                        td.classList = 'kurs';
                        td.innerHTML = wpd.einstieg.toFixed(2);
                        var span = _e('span');
                        span.classList = 'dec3';
                        span.innerHTML = wpd.einstieg.toFixed(3).slice(-1);
                        td.append(span);

                        var sup = _e('sup');
                        sup.innerHTML = 'EUR';
                        td.append(sup);
                        td.append(_e('br'));
                        
                        var small = _e('small');
                        small.innerHTML = wpd.einstieg_datum.toJSON().slice(0,10);
                        td.append(small);
                        tr.append(td);

                        var td = _e('td');
                        td.classList = 'kurs';
                        var small = _e('small');
                        small.innerHTML = wpd.e_wert.toFixed(2) + ' EUR<br>-' + ($('#fee').val()*1).toFixed(2) + ' EUR';
                        td.append(small);
                        tr.append(td);

                        var td = _e('td');
                        td.classList = 'kurs';
                        td.innerHTML = wpd.ende.toFixed(2);
                        var span = _e('span');
                        span.classList = 'dec3';
                        span.innerHTML = wpd.ende.toFixed(3).slice(-1);
                        td.append(span);

                        var sup = _e('sup');
                        sup.innerHTML = 'EUR';
                        td.append(sup);
                        td.append(_e('br'));
                        
                        var small = _e('small');
                        small.innerHTML = wpd.ausstieg_datum.toJSON().slice(0,10);
                        td.append(small);
                        tr.append(td);

                        var td = _e('td');
                        td.classList = 'kurs';
                        var small = _e('small');
                        small.innerHTML = wpd.w_wert.toFixed(2) + ' EUR<br>-' + ($('#fee').val()*1).toFixed(2) + ' EUR';
                        td.append(small);
                        tr.append(td);

                        var gv = wpd.total - wpd.e_wert - $('#fee').val()*1;
                        var td = _e('td');
                        td.classList = 'perf';
                        var span = _e('span');
                        span.classList = gv >= 0 ? 'pos' : 'neg';
                        span.innerHTML = gv.toFixed(2) + ' EUR';
                        td.append(span);
                        span.append(_e('br'));
                        span.append( ( gv >= 0 ? '+' : '' ) + ((wpd.total_perf_incl_fee-1)*100).toFixed(2) );
                        var sup = _e('sup');
                        sup.innerHTML = '%';
                        span.append(sup);
                        td.append(span);
                        tr.append(td);

                        $('table#trades tbody').prepend(tr);
                    }
                );
            }
            lastDat = v[0];
        }
    );
    
}

function toggleView(obj){
    if( obj.id == 'toggle_Trades' ){
        $('.top').toggleClass( 'd-none',true );
        $('#trades').toggleClass( 'd-none',false );
        $('.toggle_Kursperformance').toggleClass('d-none',true);
        $('.toggle_Positionsperformance').toggleClass('d-none',false);
    } else {
        $('.top').toggleClass( 'd-none',false );
        $('#trades').toggleClass( 'd-none',true );
        $('.toggle_Kursperformance').toggleClass('d-none',obj.id != 'toggle_Kursperformance');
        $('.toggle_Positionsperformance').toggleClass('d-none',obj.id != 'toggle_Positionsperformance');
    }
    $('#startposition, #fee').attr('disabled', obj.id == 'toggle_Kursperformance'); 
    $('.btn.selected').toggleClass('selected');
    $(obj).toggleClass('selected');
}