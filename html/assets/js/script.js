---
--- 
var W = {{ site.data.watchlist | jsonify }};
//var K = {{ site.data.kurs | jsonify }};
var K = content_load('kurse.json');
var R = {};
var RR = {};
var _e = (el) => ( document.createElement(el) );

$(document).ready(
    function(){

        Object.entries( W.aktien ).forEach(
            function(v,i){
                if( v[1].rating ){
                    Object.entries(v[1].rating).forEach(
                        function(w,j){
                            if( R[w[0]] == undefined ) R[w[0]] = {}
                            R[w[0]][v[0]] = w[1];
                        }
                    );
                }
               
            }
        );

        Object.entries(R).sort().reverse().forEach( 
            function(v,i){
                if( RR[v[0]] == undefined ) RR[v[0]] = [];
                Object.entries(v[1]).forEach(
                    function(w,j){
                        var fv = ((w[1][7]/w[1][1]*1)-1)*100;
                        var fv_rating = fv < 0 ? 0 : Math.log10( fv ) - 1;
                        var rat = w[1][3]*{{ site.data.rating.highflyer }} + w[1][4]*{{ site.data.rating.tradingview }} + w[1][5]*{{ site.data.rating.chartsituation }} + w[1][6]*{{ site.data.rating.newsflow }} + fv_rating;
                        RR[v[0]].push( [ w[0], w[1] , rat, ( ( i == 0 && K[ W.aktien[ w[0] ].isin ] != undefined ) ? K[ W.aktien[w[0]].isin ] : w[1][1] ) / w[1][0] * rat ] );
                    }    
                );
            }
        );
        
        Object.entries(RR).forEach(
            function(v){
                RR[v[0]].sort((a,b) => ( b[2] - a[2] ));
            }    
        )
        
        var dat = Object.entries(RR)[0][0].slice(1,5) + '-' + Object.entries(RR)[0][0].slice(5,7) + '-' + Object.entries(RR)[0][0].slice(7,9);
        var Dat = Object.entries(RR)[0][0];
        Object.entries(RR)[0][1].forEach(
            function(v,i){
                
                var aktie = W.aktien[v[0]];
                var aktie_id = v[0];

                var tr = _e('tr');
                var td = _e('td');
                td.innerHTML = aktie.startdatum + '<br><small title="ExTag: ' + aktie.dividende?.ex + '\n' + 'Ausz.: ' + aktie.dividende?.pay + '">' + aktie.dividende?.ex + '</small>';
                tr.append(td);

            
                var td = _e('td');
                td.classList='position';

                // Ranking & Score
                var span = _e('span');
                span.innerHTML = '<small class="ranking">' + (i+1) + '. </small> '+v[2].toFixed(2)+'<br>';
                span.classList = 'score';
                td.append(span);

                // Highflyer 
                var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                svg.setAttribute('width', 15);
                svg.setAttribute('height', 15);
                
                var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                rect.setAttribute('fill', v[1][3] == 1 ? 'darkseagreen' : ( v[1][3] == 0.5 ? 'darkslategrey' : 'black' ) );
                rect.setAttribute('width', 15);
                rect.setAttribute('height', 15);
                rect.innerHTML='<title>Abstand zum 52W Hoch?</title>';
 
                svg.appendChild(rect);
                td.append(svg);
                td.append(' ');

                // TV rating
                var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                svg.setAttribute('width', 15);
                svg.setAttribute('height', 15);

                var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                rect.setAttribute('fill', v[1][4] == 1 ? 'darkseagreen' : ( v[1][4] == 0.5 ? 'darkslategrey' : 'black' ) );
                rect.setAttribute('width', 15);
                rect.setAttribute('height', 15);
                rect.innerHTML='<title>Monatsperformance?</title>';
                svg.appendChild(rect);
                td.append(svg);
                td.append(' ');

                // Chart
                var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                svg.setAttribute('width', 15);
                svg.setAttribute('height', 15);

                var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                rect.setAttribute('fill', v[1][5] == 1 ? 'darkseagreen' : ( v[1][5] == 0.5 ? 'darkslategrey' : 'black' ) );
                rect.setAttribute('width', 15);
                rect.setAttribute('height', 15);
                rect.innerHTML='<title>Abstand zum gleitenden Durchschnitt (50/200)?</title>';
                svg.appendChild(rect);
                td.append(svg);
                td.append(' ');

                // Fundamental
                var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                svg.setAttribute('width', 15);
                svg.setAttribute('height', 15);

                var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                rect.setAttribute('fill', v[1][6] >= 0.8 ? 'darkseagreen' : ( v[1][6] >= 0.5 ? 'darkslategrey' : 'black' ) );
                rect.setAttribute('width', 15);
                rect.setAttribute('height', 15);
                rect.innerHTML='<title>Bewertung bei Marketscreener?</title>';
                svg.appendChild(rect);
                td.append(svg);
                td.append(' ');
                
                tr.append(td);

                // Kurs
                var kurs = K[ W.aktien[v[0]].isin] ? K[ W.aktien[v[0]].isin] : v[1][1];
                var td = _e('td');
                td.classList='kurs';
                td.innerHTML = kurs?.toFixed(3).slice(0,-1) + '<span class="dec3">' + kurs?.toFixed(3).slice(-1) + '</span><sup>' + v[1][2] + '</sup>';
                tr.append(td);

                // Perf market
                var startdat = 'D' + aktie.startdatum.replace(/-/g,'');
                if( W.waehrungen['EUR' + aktie.startwaehrung][startdat] == undefined ) console.log( startdat, aktie.startwaehrung );
                var startkurs_EUR = aktie.startkurs / W.waehrungen['EUR' + aktie.startwaehrung][startdat];
                if( W.waehrungen['EUR' + v[1][2]][Dat] == undefined ) console.log( Dat, v[1][2] );
                var endkurs_EUR = kurs / W.waehrungen['EUR' + v[1][2]][Dat];
                var td = _e('td');
                td.classList = 'eng performance ';
                var perf = ( ( ( endkurs_EUR / startkurs_EUR ) - 1 ) * 100 );
                td.innerHTML = ( perf >= 0 ? '+' : '' ) + ( ( ( endkurs_EUR / startkurs_EUR ) - 1 ) * 100 ).toFixed(2) + '<sup>%</sup><br>';
                td.classList += perf >= 0 ? 'pos' : 'neg'; 
                td.title = startkurs_EUR.toFixed(3) + ' EUR - ' + endkurs_EUR.toFixed(3) + ' EUR';
                var small = _e('small');
                small.innerHTML = aktie.startkurs.toFixed(3) + ' ' + aktie.startwaehrung;
                td.append(small);
                tr.append(td);

                // Unternehmen und Tags 
                var td = _e('td');
                td.classList='anlage';
                var ahref= _e('a');
                ahref.href='./erweitert.html#' + aktie_id;
                ahref.innerHTML = aktie.anlage;
                td.append(ahref);
                td.append(' ');

                var ahref= _e('a');
                ahref.href=aktie.url.onvista;
                ahref.target='info';
                ahref.innerHTML = '<i class="onvista fa-solid fa-link"></i>';
                td.append(ahref);
                td.append(' ');

                var ahref= _e('a');
                ahref.href=aktie.url.tradingview;
                ahref.target='info';
                ahref.innerHTML = '<i class="tradingview fa-solid fa-link"></i>';
                td.append(ahref);
                td.append(_e('br'));

                var small = _e('small');
                small.classList = 'tags';
                small.innerHTML = aktie.tags;
                td.append(small);
                tr.append(td);

                // Numbers
                var dd = [];
                var dd_1 = 2027;
                var dd_arr = [ 0,1,2,3 ];
                dd_arr.forEach(
                    function(v){
                        dd.push( aktie.numbers.filter((a) => (a[0] == dd_1 - v ))[0] );
                    }
                );

                // Umsatzwachstum
                dd_arr.slice(0,-1).forEach(
                    function(v){
                        var td = _e('td');
                        //var u = (dd[v] && dd[v+1]) ? 100 * ( ( dd[v][1] / dd[v+1][1] ) - 1 ) : 9999;
                        var u = dd[v][1];
                        td.classList='uwachstum ' + ( u > 0 && u != 9999 ? 'pos' : 'neg' );
                        td.innerHTML = u == 9999 ? 'na' : u.toFixed(0) + '%';
                        tr.append(td);
                    }
                );

                // Fair Value Offset
                var gg = dd.slice(-3,-2)[0];
                if( gg != undefined ){
                    //var gew = gg[2];
                    //var gew_neg = gew <= 0;
                    //gew = gew_neg ? dd.slice(-3,-2)[0][1] * 0.1 / aktie.aktien : gew;
                    //var waehrung_kurs = Object.values(aktie.rating)[0][2];
                    //console.log( waehrung_kurs+aktie.waehrung )
                    //var waehrung_umr = ( waehrung_kurs != aktie.waehrung ) ? W.waehrungen[ waehrung_kurs+aktie.waehrung ][0] : 1;
                    //var kgv_forward = kurs * waehrung_umr / gew;
                    //var u_growth = Math.pow( dd[0][1] / dd.slice(-1)[0][1] , 1/(dd.length-1) );
                    //var fv_offset =  ( (u_growth - 1 ) * 100 * 2 ) / kgv_forward;
                    var fv_offset_price = Object.values(aktie.rating)[0][7];
                    var fv_offset = fv_offset_price/kurs;
                    var td = _e('td');
                    td.classList='fairvalue ' + ( fv_offset >= 1 && fv_offset != 9999 ? 'pos' : 'neg' );
                    //td.innerHTML = u_growth < 1 ? 'neg' : (( fv_offset >= 1 ) ? '+' : '' ) + ((fv_offset - 1)*100).toFixed(0) + '%' + ( gew_neg ? '<sup>*</sup>' : '' );
                    td.innerHTML = (( fv_offset >= 1 ) ? '+' : '' ) + ((fv_offset - 1)*100).toFixed(0) + '%';
                    tr.append(td);
                } else {
                    var td = _e('td');
                    td.classList = 'fairvalue neg';
                    td.innerHTML = 'na';
                    tr.append(td);
                }
                
                
                /*
                var d24 = aktie.numbers.filter((a) => (a[0] == 2024 ))[0];
                var d23 = aktie.numbers.filter((a) => (a[0] == 2023 ))[0];
                var d22 = aktie.numbers.filter((a) => (a[0] == 2022 ))[0];
                var d21 = aktie.numbers.filter((a) => (a[0] == 2021 ))[0];
                var d20 = aktie.numbers.filter((a) => (a[0] == 2020 ))[0];
                */ 

                // Umsatzwachstum 2024
                //var td = _e('td');
                //var u24 = (d24 && d23) ? 100 * ( ( d24[1] / d23[1]) - 1 ) : 9999;
                //td.classList='uwachstum ' + ( u24 > 0 && u24 != 9999 ? 'pos' : 'neg' );
                //td.innerHTML = u24 == 9999 ? 'na' : u24.toFixed(0) + '%';
                //tr.append(td);

                // Umsatzwachstum 2023
                //var td = _e('td');
                //var u23 = (d23 && d22) ? 100 * ( ( d23[1] / d22[1]) - 1 ) : 9999;
                //td.classList='uwachstum ' + ( u23 > 0 && u23 != 9999 ? 'pos' : 'neg' );
                //td.innerHTML = u23 == 9999 ? 'na' : u23.toFixed(0) + '%';
                //tr.append(td);

                // Umsatzwachstum 2022
                //var td = _e('td');
                //var u22 = (d22 && d21) ? 100 * ( ( d22[1] / d21[1]) - 1 ) : 9999;
                //td.classList='uwachstum ' + ( u22 > 0 && u22 != 9999 ? 'pos' : 'neg' );
                //td.innerHTML = u22 == 9999 ? 'na' : u22.toFixed(0) + '%';
                //tr.append(td);

                // Umsatzwachstum 2021
                //var td = _e('td');
                //var u21 = (d21 && d20) ? 100 * ( ( d21[1] / d20[1]) - 1 ) : 9999;
                //td.classList='uwachstum ' + ( u21 > 0 && u21 != 9999 ? 'pos' : 'neg' );
                //td.innerHTML = u21 == 9999 ? 'na' : u21.toFixed(0) + '%';
                //tr.append(td);

                // Kommentar 
                var td = _e('td');
                td.innerHTML = aktie.kommentar;
                td.classList = 'kommentar';
                tr.append(td);

                $('#aktien tbody').append(tr);
            }    
        );

        Object.entries(W.selektoren).forEach(
            function(v){

                var aktie_id = v[0];
                var aktie = v[1];
                var dt = Object.keys(R).sort().reverse()[0];
                
                if( aktie.rating[dt] == undefined ) return;
                if( !aktie.watchlist ) return;

                var rating = aktie.rating[dt];

                var tr = _e('tr');
                var td = _e('td');
                td.innerHTML = aktie.startdatum;
                tr.append(td);

            
                var td = _e('td');
                td.classList='position';

                td.append('');
                tr.append(td);

                // Kurs
                var kurs = K[ W.selektoren[aktie_id].isin] ? K[ W.selektoren[aktie_id].isin] : rating[1];
                var td = _e('td');
                td.classList='kurs';
                td.innerHTML = kurs?.toFixed(3).slice(0,-1) + '<span class="dec3">' + kurs?.toFixed(3).slice(-1) + '</span><sup>' + rating[2] + '</sup>';
                tr.append(td);

                // Perf market
                var td = _e('td');
                td.classList = 'eng performance ';
                var perf = ( ( ( kurs / aktie.startkurs ) - 1 ) * 100 );
                td.innerHTML = ( perf >= 0 ? '+' : '' ) + ( ( ( kurs / aktie.startkurs ) - 1 ) * 100 ).toFixed(2) + '<sup>%</sup><br>';
                td.classList += perf >= 0 ? 'pos' : 'neg'; 
                td.title = aktie.startkurs.toFixed(3) + ' ' + aktie.startwaehrung + ' - ' + kurs.toFixed(3) + ' ' + rating[2];
                var small = _e('small');
                small.innerHTML = aktie.startkurs.toFixed(3) + ' ' + rating[2];
                td.append(small);
                tr.append(td);

                // Unternehmen und Tags 
                var td = _e('td');
                td.classList='anlage';
                var ahref= _e('a');
                ahref.href='./erweitert.html#' + aktie_id;
                ahref.innerHTML = aktie.anlage;
                td.append(ahref);
                td.append(' ');

                var ahref= _e('a');
                ahref.href=aktie.url.onvista;
                ahref.target='info';
                ahref.innerHTML = '<i class="onvista fa-solid fa-link"></i>';
                td.append(ahref);
                td.append(' ');

                var ahref= _e('a');
                ahref.href=aktie.url.tradingview;
                ahref.target='info';
                ahref.innerHTML = '<i class="tradingview fa-solid fa-link"></i>';
                td.append(ahref);
                td.append(_e('br'));

                var small = _e('small');
                small.classList = 'tags';
                small.innerHTML = aktie.tags;
                td.append(small);
                tr.append(td);

                // Kommentar 
                var td = _e('td');
                td.colSpan = 5;
                td.innerHTML = aktie.kommentar;
                td.classList = 'kommentar';
                tr.append(td);

                $('#selektoren tbody').append(tr);

            }
        );

        Object.entries(W.etf).forEach(
            function(v){

                var etf_id = v[0];
                var etf = v[1];
                var dt = Object.keys(R).sort().reverse()[0];

                if( etf.rating[dt] == undefined ) return;
                if( !etf.watchlist ) return;

                var tr = _e('tr');
                var td = _e('td');
                td.innerHTML = etf.startdatum;
                tr.append(td);

            
                var td = _e('td');
                td.classList='position';

                // Ranking & Score
                var rating = etf.rating[dt];
                var score = rating[3] + rating[4] + rating[5];
                var span = _e('span');
                span.innerHTML = score.toFixed(2)+'<br>';
                span.classList = 'score';
                td.append(span);


                // Highflyer 
                var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                svg.setAttribute('width', 15);
                svg.setAttribute('height', 15);
                
                var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                rect.setAttribute('fill', rating[3] == 1 ? 'darkseagreen' : ( rating[3] == 0.5 ? 'darkslategrey' : 'black' ) );
                rect.setAttribute('width', 15);
                rect.setAttribute('height', 15);
                rect.innerHTML='<title>Abstand zum 52W Hoch?</title>';
 
                svg.appendChild(rect);
                td.append(svg);
                td.append(' ');

                // TV rating
                var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                svg.setAttribute('width', 15);
                svg.setAttribute('height', 15);

                var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                rect.setAttribute('fill', rating[4] == 1 ? 'darkseagreen' : ( rating[4] == 0.5 ? 'darkslategrey' : 'black' ) );
                rect.setAttribute('width', 15);
                rect.setAttribute('height', 15);
                rect.innerHTML='<title>Monatsperformance?</title>';
                svg.appendChild(rect);
                td.append(svg);
                td.append(' ');

                // Chart
                var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                svg.setAttribute('width', 15);
                svg.setAttribute('height', 15);

                var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                rect.setAttribute('fill', rating[5] == 1 ? 'darkseagreen' : ( rating[5] == 0.5 ? 'darkslategrey' : 'black' ) );
                rect.setAttribute('width', 15);
                rect.setAttribute('height', 15);
                rect.innerHTML='<title>Abstand zum gleitenden Durchschnitt (50/200)?</title>';
                svg.appendChild(rect);
                td.append(svg);
                td.append(' ');

                tr.append(td);

                // Fundamental
                /*
                var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                svg.setAttribute('width', 15);
                svg.setAttribute('height', 15);

                var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                rect.setAttribute('fill', 'transparent' );
                rect.setAttribute('width', 15);
                rect.setAttribute('height', 15);
                rect.innerHTML='';
                svg.appendChild(rect);
                td.append(svg);
                td.append(' ');
                */

                // Kurs
                var kurs = K[ W.etf[etf_id].isin] ? K[ W.etf[etf_id].isin] : rating[1];
                var td = _e('td');
                td.classList='kurs';
                td.innerHTML = kurs?.toFixed(3).slice(0,-1) + '<span class="dec3">' + kurs?.toFixed(3).slice(-1) + '</span><sup>' + rating[2] + '</sup>';
                tr.append(td);

                // Perf market
                var td = _e('td');
                td.classList = 'eng performance ';
                var perf = ( ( ( kurs / etf.startkurs ) - 1 ) * 100 );
                td.innerHTML = ( perf >= 0 ? '+' : '' ) + ( ( ( kurs / etf.startkurs ) - 1 ) * 100 ).toFixed(2) + '<sup>%</sup><br>';
                td.classList += perf >= 0 ? 'pos' : 'neg'; 
                td.title = etf.startkurs.toFixed(3) + ' ' + rating[2] + ' - ' + kurs.toFixed(3) + ' ' + rating[2];
                var small = _e('small');
                small.innerHTML = etf.startkurs.toFixed(3) + ' ' + rating[2];
                td.append(small);
                tr.append(td);

                // Unternehmen und Tags 
                var td = _e('td');
                td.classList='anlage';
                var ahref= _e('a');
                ahref.href='./erweitert.html#' + etf_id;
                ahref.innerHTML = etf.anlage;
                td.append(ahref);
                td.append(' ');

                var ahref= _e('a');
                ahref.href=etf.url.onvista;
                ahref.target='info';
                ahref.innerHTML = '<i class="onvista fa-solid fa-link"></i>';
                td.append(ahref);
                td.append(' ');

                var ahref= _e('a');
                ahref.href=etf.url.tradingview;
                ahref.target='info';
                ahref.innerHTML = '<i class="tradingview fa-solid fa-link"></i>';
                td.append(ahref);
                td.append(_e('br'));

                var small = _e('small');
                small.classList = 'tags';
                small.innerHTML = etf.tags;
                td.append(small);
                tr.append(td);

                // Kommentar 
                var td = _e('td');
                td.colSpan = 5;
                td.innerHTML = etf.kommentar;
                td.classList = 'kommentar';
                tr.append(td);

                $('#etf tbody').append(tr);

            }
        );
    }
);