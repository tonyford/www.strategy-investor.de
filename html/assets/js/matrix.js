---
--- 

var DAX = {{ site.data.DAX | jsonify }};
var W = {{ site.data.watchlist | jsonify }};
//var K = {{ site.data.kurs | jsonify }};
var K = content_load('kurse.json');
var R = {};
var RR = {};
const RECT_SIZE=10;

var _e = (el) => ( document.createElement(el) );

var Aktien = [];
var lastAktien = [];

// Musterdepot per Datum
var WPD={};


Array.prototype.diff = function(arr2) { 
    return this.filter(x => !arr2.includes(x)); 
  }

Array.prototype.min = function(arr2) {
    return this.reduce( (a,b) => a > b ? b : a );
}

Array.prototype.max = function(arr2) {
    return this.reduce( (a,b) => a < b ? b : a );
}

$(document).ready(
    function(){

        Object.entries( W.aktien ).concat( Object.entries( W.aktien_archiv ) ).forEach(
            function(v,i){
                if( v[1].rating ){
                    Object.entries(v[1].rating).forEach(
                        function(w,j){
                            if( R[w[0]] == undefined ) R[w[0]] = {}
                            R[w[0]][v[0]] = w[1] ;
                        }
                    );
                }
               
            }
        );

        Object.entries(R).forEach( 
            function(v,i){
                if( RR[v[0]] == undefined ) RR[v[0]] = [];
                Object.entries(v[1]).forEach(
                    function(w,j){
                        RR[v[0]].push( [ w[0], w[1], w[1][3]*{{ site.data.rating.highflyer }} + w[1][4]*{{ site.data.rating.tradingview }} + w[1][5]*{{ site.data.rating.chartsituation }} + w[1][6]*{{ site.data.rating.newsflow }} + w[1][7], w[1][3], w[1][4], w[1][5] ] ); 
                    }    
                );
            }
        );
        Object.entries(RR).forEach(
            function(v){
                RR[v[0]].sort((a,b) => ( b[2] - a[2] ));
                if( W.top[v[0]] != undefined ){
                    var toplist=W.top[v[0]].map((c) => ( RR[v[0]].filter((d) => (d[0] == c )) ) ).map((c) => (c[0]));
                    RR[v[0]] = toplist.concat( RR[v[0]].diff( toplist ));
                }
            }    
        );
        
        calcStatistik();
        //calcPerformance();
        //getTrades();
    }
);

function calcPerformance(){


    var DPerf=1;
    var DAXstart=1;
    var DAXende=1; 
    var lastDat=undefined;
    $('table.top tbody').html('');

    Object.entries(RR).sort().forEach(
        function(w,j,array){
            var WP = [];

            // Musterdepot fuer Datum erstellen
            WPD[w[0]] = { 'cash' : j == 0 ? $('#startposition').val() * TOP : WPD[lastDat].cash };
            var Wpd = WPD[w[0]];

            lastAktien = Aktien;
            Aktien=[];

            
            var dat = w[0].slice(1,5) + '-' + w[0].slice(5,7) + '-' + w[0].slice(7,9);

            if( j == 0 ){
                DAXstart = DAX[w[0]][0];
                DAXstart_dat = dat;
            } 
            var tr = _e('tr');
            var td = _e('td');
            td.classList = 'dat'
            td.innerHTML=dat;
            tr.append(td);
            var TD = w[1].filter((a,b) => (b<TOP)).map(
                function(v,i){
                    var aktie = W.aktien[v[0]];
                    var aktie_id = v[0];
                    Aktien.push(aktie_id);

                    // Aktie in Musterdepot schreiben
                    WPD[w[0]][v[0]]={} 
                    var wpd = WPD[w[0]][v[0]];
                    var last_wpd = lastDat != undefined ? WPD[lastDat][v[0]] : undefined;

                    //var einstieg = lastAktien.indexOf(aktie_id) == -1;
                    var einstieg = lastDat != undefined ? WPD[lastDat][v[0]] == undefined : true;

                    wpd['start'] = v[1][0];
                    var kurs = K[aktie.isin] ? K[aktie.isin] : v[1][1];
                    wpd['ende'] = j === array.length - 1 ? kurs : v[1][1];
                    wpd['einstieg'] = einstieg ? v[1][0] : last_wpd.einstieg;
                    wpd['einstieg_datum'] = einstieg ? new Date(w[0].slice(1,5) + '-' + w[0].slice(5,7) + '-' + w[0].slice(7,9) + 'UTC' ) : last_wpd.einstieg_datum;
                    wpd['woche_im_depot'] = einstieg ? 1 : last_wpd.woche_im_depot + 1; 
                    wpd['w_perf'] = wpd.ende / wpd.start;
                    wpd['e_perf'] = wpd.ende / wpd.einstieg;
                    wpd['stk'] = einstieg ? parseInt( ($('#startposition').val()*1-$('#fee').val()*1) / wpd.einstieg ) : last_wpd.stk;
                    wpd['e_wert'] = einstieg ? wpd.einstieg * wpd.stk : last_wpd.e_wert;
                    wpd['w_wert'] = wpd.stk * wpd.ende;
                    wpd['divi_stk'] = $.isNumeric(v[1][8]) ? v[1][8] : 0;
                    wpd['dividende'] = wpd.divi_stk * wpd.stk;
                    Wpd.cash -= einstieg ? wpd.e_wert + $('#fee').val()*1 : 0; 
                    wpd['total'] = wpd.w_wert + wpd.dividende - $('#fee').val()*1;
                    wpd['total_perf_incl_fee'] = wpd.total / wpd.e_wert;
                    //wpd['ausstieg_datum'] = new Date(w[0].slice(1,5) + '-' + w[0].slice(6,7) + '-' + w[0].slice(8,9) );

                    var td = _e('td');
                    td.classList='position eng';

                    // Ranking & Score
                    var spann = _e('span');
                    spann.classList = 'score';
                    WP.push( ( j === array.length - 1 ? kurs : v[1][1] ) / v[1][0]);

                    var dividende = v[1][8] ? v[1][8] : 0;
                    var perf = 100*(( ( ( j === array.length - 1 ? kurs : v[1][1] ) + dividende ) / v[1][0])-1);

                    var span = _e('span');
                    span.classList = 'compact '+ ( einstieg ? 'einstieg' : '' );
                    span.innerHTML = aktie.anlage.slice(0,14)+'<sup>' + wpd.woche_im_depot + '</sup><br>';
                    spann.append(span);

                    
                    var span = _e('span');
                    span.classList = 'toggle_Positionsperformance perff ' + ( wpd.total_perf_incl_fee >= 1 ? 'pos' : 'neg' );
                    span.title = 'Kalkulierte Positionsperformance seit Einstieg unter Einberechnung der Transaktionsgebühren';
                    var spa = _e('span');
                    spa.classList = 'runter';
                    spa.innerHTML = ( wpd.total_perf_incl_fee >= 1 ? '+' : '' ) + (100*(wpd.total_perf_incl_fee-1)).toFixed(2) +'%';
                    span.append(spa);
                    span.append(_e('br'));
                    var small = _e('small');
                    small.innerHTML = wpd.einstieg.toFixed(3) + ' - ' + ( j === array.length - 1 ? kurs : v[1][1] ).toFixed(3);
                    small.title = 'Positionsperformance\n' + wpd.einstieg.toFixed(3) + ' ' + v[1][2] + ' - ' + ( j === array.length - 1 ? kurs : v[1][1] ).toFixed(3) + ' ' + v[1][2];
                    span.append(small);
                    span.append(_e('br'));

                    spann.append(span);

                    var span = _e('span');
                    span.classList = 'kursperformance toggle_Kursperformance '+ ( perf >= 0 ? 'pos' : 'neg' );
                    span.title = 'Kursperformance\n' + v[1][0].toFixed(3) + ' ' + v[1][2] + ' - ' + ( j === array.length - 1 ? kurs : v[1][1] ).toFixed(3) + ' ' + v[1][2];
                    //span.innerHTML = ((perf >= 0) ? '+' : '') +perf.toFixed(2)+'%' + (( dividende > 0 ? '<sub>Divi</sub>' : '' ))+ '<br>';
                    var spa = _e('span');
                    spa.classList = 'runter';
                    spa.title = span.title; 
                    spa.innerHTML = ((perf >= 0) ? '+' : '') +perf.toFixed(2)+'%' + (( dividende > 0 ? '<sub>Divi</sub>' : '' ));
                    span.append(spa);
                    span.append(_e('br'));
                    var small = _e('small');
                    small.innerHTML = v[1][0].toFixed(3) + ' - ' + ( j === array.length - 1 ? kurs : v[1][1] ).toFixed(3);
                    span.append(small);
                    span.append(_e('br'));
                    
                    spann.append(span);

                    var small = _e('small');
                    small.classList = 'ranking toggle_Kursperformance';
                    small.title = 'Rang';
                    small.innerHTML = (i+1);
                    spann.append(small);

                    spann.append(' ');
                    
                    var span = _e('span');
                    span.classList = 'toggle_Kursperformance';
                    span.title = 'Meine Wertung';
                    span.innerHTML = v[2].toFixed(2)+'<br>';
                    spann.append(span);

                    var span = _e('span');
                    span.classList = 'stk toggle_Positionsperformance';
                    span.title = 'Kalkulierte Stückzahl';
                    span.innerHTML =  wpd.stk + ' stk<br>';
                    spann.append(span);


                    td.append(spann);

                    // Highflyer 
                    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                    svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                    svg.setAttribute('width', RECT_SIZE);
                    svg.setAttribute('height', RECT_SIZE);
                    
                    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                    rect.setAttribute('fill', v[1][3] == 1 ? 'darkseagreen' : 'black' );
                    rect.setAttribute('width', RECT_SIZE);
                    rect.setAttribute('height', RECT_SIZE);
                    rect.innerHTML='<title>Highflyer?</title>';
    
                    svg.appendChild(rect);
                    td.append(svg);
                    td.append(' ');

                    // TV rating
                    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                    svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                    svg.setAttribute('width', RECT_SIZE);
                    svg.setAttribute('height', RECT_SIZE);

                    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                    rect.setAttribute('fill', v[1][4] == 1 ? 'darkseagreen' : 'black' );
                    rect.setAttribute('width', RECT_SIZE);
                    rect.setAttribute('height', RECT_SIZE);
                    rect.innerHTML='<title>Tradingview?</title>';
                    svg.appendChild(rect);
                    td.append(svg);
                    td.append(' ');

                    // Chart
                    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                    svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                    svg.setAttribute('width', RECT_SIZE);
                    svg.setAttribute('height', RECT_SIZE);

                    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                    rect.setAttribute('fill', v[1][5] == 1 ? 'darkseagreen' : 'black' );
                    rect.setAttribute('width', RECT_SIZE);
                    rect.setAttribute('height', RECT_SIZE);
                    rect.innerHTML='<title>Chartsituation?</title>';
                    svg.appendChild(rect);
                    td.append(svg);
                    td.append(' ');

                    // Fundamental
                    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                    svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
                    svg.setAttribute('width', RECT_SIZE);
                    svg.setAttribute('height', RECT_SIZE);

                    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
                    rect.setAttribute('fill', v[1][6] == 1 ? 'darkseagreen' : 'black' );
                    rect.setAttribute('width', RECT_SIZE);
                    rect.setAttribute('height', RECT_SIZE);
                    rect.innerHTML='<title>Marketscreener?</title>';
                    svg.appendChild(rect);
                    td.append(svg);

                    return td;
                }
            );

            TD.forEach((td) => (tr.append(td)));
            var WPerf = WP.reduce((a,b) => (a+b)) / WP.length;
            var DAXkurs = K.DAX && j == array.length - 1 ? K.DAX : DAX[w[0]][1];
            var DAXp = DAXkurs / DAX[w[0]][0];
            var DAXpp = DAXkurs / DAXstart;
            DPerf = DPerf * WPerf;
            DAXende = DAXkurs;
            DAXende_dat = dat;
            var td = _e('td');

            if( j > 0 ){
                var WerteW = Object.keys(Wpd);
                var WerteVW = Object.keys(WPD[lastDat] );
                var WerteDiff = WerteVW.diff(WerteW);
                WerteDiff.forEach(
                    function(z){
                        Wpd.cash += WPD[lastDat][z].total ? WPD[lastDat][z].total : 0;
                        WPD[lastDat][z].ausstieg_datum = new Date(w[0].slice(1,5) + '-' + w[0].slice(5,7) + '-' + w[0].slice(7,9) + 'UTC' );                      
                    }
                );
            }

            var total_perf = Object.values(Wpd).map((a) => (a.total ? a.total : 0 )).reduce((a,b) => (a+b)) + Wpd.cash;
            total_perf = total_perf / ( $('#startposition').val() * TOP );
            Wpd['total_perf'] = total_perf;
            td.classList = 'perform ';
            td.classList += WPerf >= 1 ? 'pos' : 'neg';

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance perff ' + (total_perf >= 1 ? 'pos' : 'neg' );
            span.innerHTML = (total_perf >= 1 ? '+' : '' ) + (100*(total_perf-1)).toFixed(2) + '%<br>';
            td.append(span);

            var span = _e('span');
            span.classList = 'kursperformance toggle_Kursperformance';
            span.title = 'Mittlere Kursperformance';
            span.innerHTML = ( WPerf >= 1 ? '+' : '' ) + (( WPerf - 1 ) * 100 ).toFixed(2) + '<sup>%</sup><br>';
            td.append(span);

            var small = _e('small');
            small.classList = 'toggle_Kursperformance ' +( DAXp >= 1 ? 'pos' : 'neg');
            small.innerHTML = 'DAX ' +(DAXp >= 1 ? '+' : '')+ (100*(DAXp-1)).toFixed(2);
            td.append(small);

            var small = _e('small');
            small.classList = 'toggle_Positionsperformance ' +( DAXpp >= 1 ? 'pos' : 'neg');
            small.innerHTML = 'DAX ' +(DAXpp >= 1 ? '+' : '')+ (100*(DAXpp-1)).toFixed(2);
            td.append(small);

            tr.append(td);
                
            $('table.top tbody').prepend(tr);
            lastDat=w[0];
        }
    );
    DPerf_all = DAXende / DAXstart;

    $('#performance').html('');

    var span = _e('span');
    span.classList = 'toggle_Positionsperformance ' + ( WPD[lastDat].total_perf >= 1 ? 'pos' : 'neg' );
    span.title = 'Kalkulierte Gesamtperformance aller offenen und geschlossenen Positionen';
    span.innerHTML = ( WPD[lastDat].total_perf >= 1 ? '+' : '' ) + (100*(WPD[lastDat].total_perf-1)).toFixed(2) + '%<br>';
    $('#performance').append(span);

    var span = _e('span');
    span.classList = 'kursperformance toggle_Kursperformance';
    span.title = 'Multiplizierte mittlere Wochen-Kursperformance';
    span.innerHTML = ( DPerf >= 1 ? '+' : '' ) + (100*(DPerf-1)).toFixed(2)+'%<br>';
    $('#performance').append(span);
    
    var small = _e('small');
    small.classList = ( DPerf_all >= 1 ? 'pos' : 'neg' );
    small.title = 'DAX-Performance\n' + DAXstart_dat + ': ' + DAXstart.toFixed(0) + ' Pkt\n' + DAXende_dat + ': ' + DAXende.toFixed(0) + ' Pkt';
    small.innerHTML = 'DAX '+ (DPerf_all >= 1 ? '+' : '') + (100*(DPerf_all-1)).toFixed(2) +'%';
    $('#performance').append(small);

    $('#performance').addClass( DPerf >= 1 ? 'pos' : 'neg' );
    
    $('.btn.selected').click();

}


function toggleView(obj){
    $('.top').toggleClass( 'd-none',false );
    $('#trades').toggleClass( 'd-none',true );
    $('.toggle_Kursperformance').toggleClass('d-none',obj.id != 'toggle_Kursperformance');
    $('.toggle_Positionsperformance').toggleClass('d-none',obj.id != 'toggle_Positionsperformance');
    $('.btn.selected').toggleClass('selected');
    $(obj).toggleClass('selected');
    $('#matrix').toggleClass('Kursperformance', obj.id == 'toggle_Kursperformance' );
    $('#matrix').toggleClass('Positionsperformance', obj.id == 'toggle_Positionsperformance' );
}


var TTT=[];

function calcStatistik(){

    $('table.top tbody').html('');

    var WL = Object.entries(RR)
        .map(
            a => [ a[0], a[1].map( b => b[1][1] / b[1][0] ) ]
        ).map(
            a => [ a[0], a[1].reduce( (b,c) => b+c ) / a[1].length  ]
        );
    
    var WLg = Object.entries(RR)
        .map(
            a => [ a[0], a[1].map( b => [ (b[1][1] / b[1][0]) * b[2], b[2] ] ) ]
        ).map(
            a => [ a[0], a[1].map( b => b[0]).reduce( (b,c) => b+c ) / a[1].map( b => b[1]).reduce( (b,c) => b+c )  ]
        );

    var W1 = Object.entries(RR)
        .map(
            a => [ 
                a[0], a[1].filter( b => b[2] >= 1 && b[2] < 2 ).map(c => c[1][1] / c[1][0] )        
            ]
        ).map(
            a => [ a[0], a[1].length > 0 ? a[1].reduce( (b,c) => b+c ) / a[1].length : 1 ]
        );

    var W2 = Object.entries(RR)
        .map(
            a => [ 
                a[0], a[1].filter( b => b[2] >= 2 && b[2] < 3 ).map(c => c[1][1] / c[1][0] )        
            ]
        ).map(
            a => [ a[0], a[1].length > 0 ? a[1].reduce( (b,c) => b+c ) / a[1].length : 1 ]
        );
    
    var W3 = Object.entries(RR)
        .map(
            a => [ 
                a[0], a[1].filter( b => b[2] >= 3 && b[2] < 4 ).map(c => c[1][1] / c[1][0] )        
            ]
        ).map(
            a => [ a[0], a[1].length > 0 ? a[1].reduce( (b,c) => b+c ) / a[1].length : 1 ]
        );
    
    var W4 = Object.entries(RR)
        .map(
            a => [ 
                a[0], a[1].filter( b => b[2] >= 4 && b[2] < 5 ).map(c => c[1][1] / c[1][0] )        
            ]
        ).map(
            a => [ a[0], a[1].length > 0 ? a[1].reduce( (b,c) => b+c ) / a[1].length : 1 ]
        );
    
    var W5 = Object.entries(RR)
        .map(
            a => [ 
                a[0], a[1].filter( b => b[2] >= 5 ).map(c => c[1][1] / c[1][0] )        
            ]
        ).map(
            a => [ a[0], a[1].length > 0 ? a[1].reduce( (b,c) => b+c ) / a[1].length : 1 ]
        );
    
    var HF = Object.entries(RR)
        .map(
            a => [ 
                a[0], a[1].filter( b => b[3] >= 1 ).map(c => c[1][1] / c[1][0] )        
            ]
        ).map(
            a => [ a[0], a[1].length > 0 ? a[1].reduce( (b,c) => b+c ) / a[1].length : 1 ]
        );
    
    var noHF = Object.entries(RR)
        .map(
            a => [ 
                a[0], a[1].filter( b => b[3] < 0.5 ).map(c => c[1][1] / c[1][0] )        
            ]
        ).map(
            a => [ a[0], a[1].length > 0 ? a[1].reduce( (b,c) => b+c ) / a[1].length : 1 ]
        );
    
    var TV = Object.entries(RR)
        .map(
            a => [ 
                a[0], a[1].filter( b => b[4] >= 1 ).map(c => c[1][1] / c[1][0] )        
            ]
        ).map(
            a => [ a[0], a[1].length > 0 ? a[1].reduce( (b,c) => b+c ) / a[1].length : 1 ]
        );
    
    var HFTV = Object.entries(RR)
        .map(
            a => [ 
                a[0], a[1].filter( b => b[3] >= 1 && b[4] >= 1 ).map(c => c[1][1] / c[1][0] )        
            ]
        ).map(
            a => [ a[0], a[1].length > 0 ? a[1].reduce( (b,c) => b+c ) / a[1].length : 1 ]
        );

    var SI = Object.entries(RR)
        .map(
            a => [ 
                a[0], a[1].filter( b => b[5] >= 1 ).map(c => c[1][1] / c[1][0] )        
            ]
        ).map(
            a => [ a[0], a[1].length > 0 ? a[1].reduce( (b,c) => b+c ) / a[1].length : 1 ]
        );
    
    var W5HFTV = Object.entries(RR)
        .map(
            a => [ 
                a[0], a[1].filter( b => b[5] >= 1 && b[3] >= 1 && b[4] >= 1 ).map(c => c[1][1] / c[1][0] )        
            ]
        ).map(
            a => [ a[0], a[1].length > 0 ? a[1].reduce( (b,c) => b+c ) / a[1].length : 1 ]
        );

    var T1 = Object.entries(RR)
        .map(a => [ 
            a[0], a[1].filter( b => W.top[a[0]].indexOf(b[0]) > -1  ).slice(0,1) ] 
        ).map( 
            a => [ a[0], a[1].map(b => b[1][1] / b[1][0] ) ]
        ).map(a => [ a[0], a[1].reduce( (b,c) => b+c) / a[1].length ]);
    
    var T5 = Object.entries(RR)
        .map(a => [ 
            a[0], a[1].filter( b => W.top[a[0]].indexOf(b[0]) > -1  ).slice(0,5) ] 
        ).map( 
            a => [ a[0], a[1].map(b => b[1][1] / b[1][0] ) ]
        ).map(a => [ a[0], a[1].reduce( (b,c) => b+c) / a[1].length ]);
    
    var T10 = Object.entries(RR)
        .map(a => [ 
            a[0], a[1].filter( b => W.top[a[0]].indexOf(b[0]) > -1  ).slice(0,10) ] 
        ).map( 
            a => [ a[0], a[1].map(b => b[1][1] / b[1][0] ) ]
        ).map(a => [ a[0], a[1].reduce( (b,c) => b+c) / a[1].length ]);


    var Weeks = Object.keys(DAX).sort();
    var DAXperf = 1;
    var WLperf = 1;
    var WLgperf = 1;
    var W1perf = 1;
    var W2perf = 1;
    var W3perf = 1;
    var W4perf = 1;
    var W5perf = 1;
    var HFperf = 1;
    var noHFperf = 1;
    var TVperf = 1;
    var HFTVperf = 1;
    var SIperf = 1;
    var W5HFTVperf = 1;
    var T1perf = 1;
    var T5perf = 1;
    var T10perf = 1;

    Weeks.forEach(
        function(v,i){

            var dat = v.slice(1,5) + '-' + v.slice(5,7) + '-' + v.slice(7,9);

            var allP = [];
            var allPP = [];

            var per = DAX[v][1] / DAX[v][0];
            allP.push(per);
            DAXperf *= per;
            allPP.push(DAXperf);
            var dperf = ( per -1 )*100;

            var per = WL.filter( a => a[0] == v )[0][1];
            allP.push(per);
            WLperf *= per;
            allPP.push(WLperf);
            var wlperf = ( per - 1 )*100;

            var per = WLg.filter( a => a[0] == v )[0][1];
            allP.push(per);
            WLgperf *= per;
            allPP.push(WLgperf);
            var wlgperf = ( per - 1 )*100;

            var per = W1.filter( a => a[0] == v )[0][1];
            allP.push(per);
            W1perf *= per;
            allPP.push(W1perf);
            var w1perf = ( per - 1 )*100;

            var per = W2.filter( a => a[0] == v )[0][1];
            allP.push(per);
            W2perf *= per;
            allPP.push(W2perf);
            var w2perf = ( per - 1 )*100;

            var per = W3.filter( a => a[0] == v )[0][1];
            allP.push(per);
            W3perf *= per;
            allPP.push(W3perf);
            var w3perf = ( per - 1 )*100;

            var per = W4.filter( a => a[0] == v )[0][1];
            allP.push(per);
            W4perf *= per;
            allPP.push(W4perf);
            var w4perf = ( per - 1 )*100;

            var per = W5.filter( a => a[0] == v )[0][1];
            allP.push(per);
            W5perf *= per;
            allPP.push(W5perf);
            var w5perf = ( per - 1 )*100;

            var per = HF.filter( a => a[0] == v )[0][1];
            allP.push(per);
            HFperf *= per;
            allPP.push(HFperf);
            var hfperf = ( per - 1 )*100;

            var per = noHF.filter( a => a[0] == v )[0][1];
            allP.push(per);
            noHFperf *= per;
            allPP.push(noHFperf);
            var nohfperf = ( per - 1 )*100;

            var per = TV.filter( a => a[0] == v )[0][1];
            allP.push(per);
            TVperf *= per;
            allPP.push(TVperf);
            var tvperf = ( per - 1 )*100;

            var per = HFTV.filter( a => a[0] == v )[0][1];
            allP.push(per);
            HFTVperf *= per;
            var hftvperf = ( per - 1 )*100;

            var per = W5HFTV.filter( a => a[0] == v )[0][1];
            allP.push(per);
            W5HFTVperf *= per;
            allPP.push(W5HFTVperf);
            var w5hftvperf = ( per - 1 )*100;

            var per = SI.filter( a => a[0] == v )[0][1];
            allP.push(per);
            SIperf *= per;
            allPP.push(SIperf);
            var siperf = ( per - 1 )*100;

            var per = T1.filter( a => a[0] == v )[0][1];
            allP.push(per);
            T1perf *= per;
            allPP.push(T1perf);
            var t1perf = ( per - 1 )*100;

            var per = T5.filter( a => a[0] == v )[0][1];
            allP.push(per);
            T5perf *= per;
            allPP.push(T5perf);
            var t5perf = ( per - 1 )*100;

            var per = T10.filter( a => a[0] == v )[0][1];
            allP.push(per);
            T10perf *= per;
            allPP.push(T10perf);
            var t10perf = ( per - 1 )*100;

            var tr = _e('tr');
            var td = _e('td');
            td.classList = 'dat';
            td.innerHTML = dat;
            tr.append(td);
            
            // DAX
            var td = _e('td');
            td.classList = 'matrix perf'; 
            var perf = dperf; 
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == DAXperf ? ' pworst' : '' ) + ( allPP.max() == perf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (DAXperf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // Watchlist
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = wlperf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == WLperf ? ' pworst' : '' ) + ( allPP.max() == WLperf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (WLperf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // Watchlist gewichtet
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = wlgperf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == WLgperf ? ' pworst' : '' ) + ( allPP.max() == WLgperf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (WLgperf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // W1
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = w1perf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == W1perf ? ' pworst' : '' ) + ( allPP.max() == W1perf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (W1perf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // W2
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = w2perf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == W2perf ? ' pworst' : '' ) + ( allPP.max() == W2perf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (W2perf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // W3
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = w3perf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == W3perf ? ' pworst' : '' ) + ( allPP.max() == W3perf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (W3perf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // W4
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = w4perf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == W4perf ? ' pworst' : '' ) + ( allPP.max() == W4perf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (W4perf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // W5
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = w5perf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == W5perf ? ' pworst' : '' ) + ( allPP.max() == W5perf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (W5perf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // Highflyer
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = hfperf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == HFperf ? ' pworst' : '' ) + ( allPP.max() == HFperf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (HFperf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // no Highflyer
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = nohfperf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == noHFperf ? ' pworst' : '' ) + ( allPP.max() == noHFperf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (noHFperf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // TradingView
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = tvperf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == TVperf ? ' pworst' : '' ) + ( allPP.max() == TVperf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (TVperf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // Highflyer & Tradingview
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = hftvperf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == HFTVperf ? ' pworst' : '' ) + ( allPP.max() == HFTVperf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (HFTVperf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // Strategy-Investor Chartanalyse
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = siperf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == SIperf ? ' pworst' : '' ) + ( allPP.max() == SIperf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (SIperf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // W5 & HF & TV
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = w5hftvperf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == W5HFTVperf ? ' pworst' : '' ) + ( allPP.max() == W5HFTVperf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (W5HFTVperf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // Top 1
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = t1perf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == T1perf ? ' pworst' : '' ) + ( allPP.max() == T1perf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (T1perf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // Top 5
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = t5perf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == T5perf ? ' pworst' : '' ) + ( allPP.max() == T5perf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (T5perf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            // Top 10
            var td = _e('td');
            td.classList = 'matrix perf';
            var perf = t10perf;
            td.classList += ((allP.min()-1)*100 == perf ? ' kworst' : '' ) + ((allP.max()-1)*100 == perf ? ' kbest' : '' );
            td.classList += ( allPP.min() == T10perf ? ' pworst' : '' ) + ( allPP.max() == T10perf ? ' pbest' : '' );
            var span = _e('span');
            span.classList = 'toggle_Kursperformance ';
            span.classList += perf >= 0 ? 'pos' : 'neg';
            span.innerHTML = ( perf >= 0 ? '+' : '' ) + perf.toFixed(2) + '%';
            td.append(span);

            var span = _e('span');
            span.classList = 'toggle_Positionsperformance ';
            span.innerHTML = (T10perf * 100).toFixed(2);
            td.append(span);
            tr.append(td);

            $('table.top tbody').prepend(tr);

        }
    );

    

    /*
    var W4 = Object.entries(RR).map(
        (a) => (
            a.filter(
                b => b[1][2] >= 4 && b[1][2] < 5
            )
        )
    );
    */

    $('.btn.selected').click();
}