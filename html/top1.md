---
layout: default
top: 1
startposition: 1000
fee: 7.90
js: 
- assets/js/json.js
- assets/js/top.js
---
<style>
em, strong { color: aliceblue; }
</style>
<script>
    var TOP={{ page.top }};
    var STARTPOSITION={{ page.startposition }};
    var FEE={{ page.fee }};
</script>
# Strategy-Investor.de - Top 1 - Performance
{: class="fixed" }

{% include menu.md aktiv="T1" %}

Mit dem **Top 1** schicke ich jede Woche die anhand mehrerer Faktoren best bewertete Aktie ins Rennen. Mit einer kalkulierten maximalen Positionsgröße von <input type="text" id="startposition" value="{{ page.startposition }}" onchange="calcPerformance(); getTrades();">€ unter Einberechnung einer Transaktionsgebühr von <input type="text" id="fee" value="{{ page.fee | decimals }}" onchange="calcPerformance(); getTrades();">€ ( bei flatex üblich ) wird neben der theoretischen wöchentlichen Kursperformance eine realitätsnahe geringer ausfallende Wertperformance errechnet. Die Fluktuation wird versucht so gering wie möglich zu halten, weil jede Transaktionsgebühr die Performance nicht unwesentlich schmälert. Ziel ist es eine deutlich überdurchschnittliche Performance erreichen zu können.
{: style="text-align: justify;" }

<u>Keine</u> Kaufempfehlungen!<br>Jeder Anleger trägt das Risiko <u>seiner</u> Entscheidung!

<button type="button" id="toggle_Kursperformance" class="btn selected" onclick="toggleView(this)">Kursperformance</button><button type="button" id="toggle_Positionsperformance" class="btn" onclick="toggleView(this)">Positionsperformance</button>
<button type="button" id="toggle_Trades" class="btn" onclick="toggleView(this)">Trades</button>

<div id="performance" class=""></div>

<table id="top1" class="overview top">
    <thead>
        <tr>
            <th>Woche</th><th>P1</th><th>W%</th>
        </tr> 
    </thead>
    <tbody>
    </tbody>
</table>
<table id="trades" class="overview trades">
<thead>
    <tr>
        <th>Anlage</th><th>Stk</th><th colspan="2">Einstieg</th><th colspan="2">Ausstieg</th><th>Perf</th>
    </tr>
</thead>
<tbody>
</tbody>
</table>