module Decimals
    def decimals(number)
        "%.2f" % number
    end
    def perfa(number,days)
        number**(365/days)
    end
end

Liquid::Template.register_filter(Decimals) # register filter globally