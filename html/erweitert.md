---
layout: default
published: false
---
{% assign aktien = site.data.watchlist.aktien | sort %}
{% assign etfs = site.data.watchlist.etf | sort %}
# Strategy-Investor.de - Watchlist
{: class="fixed" }

{% include menu.md aktiv="E" %}

--- 
{: class="fixed" }

<small>{% for aktie in aktien %}{% if aktie[1].watchlist == true %}[{{ aktie[1].anlage }}](#{{ aktie[0] }}){% if forloop.last != true %} -{% endif %} 
{% endif %}{% endfor %}<br>{% for etf in etfs %}[{{ etf[1].anlage }}](#{{ etf[0] }}){% if forloop.last != true %} -{% endif %} 
{% endfor %}</small>
{: class="fixed" }

--- 
{: class="firstline" }


{% for aktie in aktien %}

{% if aktie[1].watchlist == true %}

[anchor](){: class="anchor" id="{{ aktie[0] }}" }

### {{ aktie[1].anlage }}{% if aktie[1].url.onvista %} <a href="{{ aktie[1].url.onvista }}" target="info"><i class="onvista fa-solid fa-link"></i></a>{% endif %}{% if aktie[1].url.tradingview %} <a href="{{ aktie[1].url.tradingview }}" target="info"><i class="tradingview fa-solid fa-link"></i></a>{% endif %}

{% if aktie[1].url.ir %}<small>[{{ aktie[1].url.ir }}]( {{ aktie[1].url.ir }} ){: target="extern" }</small>{% endif %}

{{ aktie[1].kommentar }}


| Jahr | Umsatz | GV / Aktie | BackLog |
| | <small>{{ aktie[1].zehner | default: "mio" }} {{ aktie[1].waehrung }}</small> | <small>{{ aktie[1].waehrung }}</small> | <small>{{ aktie[1].zehner | default: "mio" }} {{ aktie[1].waehrung }}</small> |
|---:|---:|---:|---:|---:|
{% for n in aktie[1].numbers %}| {% if n[4] != "" and n[4] != null %}[ {{ n[0] }} ]({{ n[4]}} ){: target="extern" }{% else %}[ {{ n[0] }} ]({{ aktie[1].url.tradingview }}){% endif %} | {{ n[1] }} | {% if n[2] != '-' %}{{ n[2] | times:1 | decimals }}{% endif %} | {{ n[3] }} |
{% endfor %}{: class="numbers"}

<table class="actions">
<tr><th>Woche</th><th>Score</th><th>Rating</th><th class="perf">Kurs</th><th style="line-height: 1em;">Perf<br><small>market</small></th><th style="text-align: left;">Kommentar</th></tr>{% if aktie[1].watchlist == true %}{% assign ratings = aktie[1].rating | sort %}
{% for p in ratings %}
    {% include weeklyscore.html p=p %}
{% endfor %}
{% endif %}
</table>

---

{% endif %}

{% endfor %}

{% for etf in etfs %}

[anchor](){: class="anchor" id="{{ etf[0] }}" }

### {{ etf[1].anlage }}{% if etf[1].url.onvista %} <a href="{{ etf[1].url.onvista }}" target="info"><i class="onvista fa-solid fa-link"></i></a>{% endif %}{% if etf[1].url.tradingview %} <a href="{{ etf[1].url.tradingview }}" target="info"><i class="tradingview fa-solid fa-link"></i></a>{% endif %}

{% if etf[1].url.ir %}<small>[{{ etf[1].url.ir }}]( {{ etf[1].url.ir }} ){: target="extern" }</small>{% endif %}

{{ etf[1].kommentar }}

<table class="actions">
<tr><th>Datum</th><th>Positionierung</th><th class="perf">Kurs</th><th style="line-height: 1em;">Perf<br><small>market</small></th><th style="line-height: 1em;">Perf<br><small>strategy</small></th><th>Trend<br><small>weekly</small></th><th>Risiko<br><small>daily</small></th><th>Kommentar</th></tr>
{% assign posts = site.posts | reverse | where: 'slug', etf[0] %}
{% assign kurs_prev = 0 %}
{% assign pos_prev = 0 %}
{% assign perf_market = 1.0 %}
{% assign perf_strategy = 1.0 %}
{% for p in posts %}
    {% if kurs_prev > 0 %}
        {% assign perf = p.kurs | divided_by: kurs_prev %}
        {% assign perf_s = perf | minus: 1.0 | times: pos_prev | plus: 1.0 %}
        {% assign perf_market = perf_market | times: perf %}
        {% assign perf_strategy = perf_strategy | times: perf_s %}
    {% endif %}
    {% assign kurs_prev = p.kurs %}
    {% assign pos_prev = p.pos %}
    
{% endfor %}
</table>

---

{% endfor %}
