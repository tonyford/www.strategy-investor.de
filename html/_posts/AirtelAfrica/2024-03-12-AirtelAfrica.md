---
layout: event
pos: 0.25
kurs: 1.13
perf: 1.0089
skurs: 1.1587
waehrung: EUR
trend: 0
risiko: -1
perspektive: langfristig
kommentar: Abwertung des nigerianischen Naira hält weiter an, daher vorsichtshalber Position deutlich reduziert
---