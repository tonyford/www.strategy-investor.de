---
layout: event
pos: 0.5
kurs: 1.20
perf: 0.9667
skurs: 1.188
waehrung: EUR
trend: 0
risiko: -1
perspektive: langfristig
kommentar: Trotz Maßnahmen gegen die Abwertung in Nigeria spitzt sich die Abwertungsspirale immer weiter zu. Ein weiterer Kursrutsch lässt sich daher nicht ausschließen. Positionierung wird mit Verlust etwas reduziert und eine Stabilisierung abgewartet.
---