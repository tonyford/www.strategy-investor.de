---
layout: event
pos: 1.0
kurs: 1.12
perf: 1.0089
skurs: 1.1484
waehrung: EUR
trend: 0
risiko: 1
perspektive: langfristig
kommentar: Kurse stabilisiert, Erholung wahrscheinlich, starke Unterstützungslinie auf aktuellem Niveau
---