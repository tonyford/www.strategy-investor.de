---
layout: event
pos: 1.0
kurs: 1.27
perf: 0.945
skurs: 1.257
waehrung: EUR
trend: 1
risiko: -1
perspektive: langfristig
kommentar: Nigeria mit Inflationsspirale, welche sich negativ auf die Ergebnisse auswirkt. Starke Unterstützungslinien sollten dennoch halten.
---
