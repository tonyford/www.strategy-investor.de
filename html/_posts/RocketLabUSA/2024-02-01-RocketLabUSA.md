---
layout: event
pos: 1.0
kurs: 3.95
skurs: 3.95
perf: 1.238
waehrung: USD
trend: 1
risiko: -1
perspektive: langfristig
kommentar: Ausgabe von Wandelanleihen erhöhen kurzfristig den Verkaufsdruck.
---
