---
layout: event
pos: 1.0
kurs: 20.10
skurs: 20.10
perf: 0.9891
waehrung: EUR
trend: 1
risiko: 0
perspektive: langfristig
kommentar: charttechnisch angeschlagen, fundamental aber sehr vielversprechend, volle Auftragsbücher und ansteigende Ergebnisse
---
