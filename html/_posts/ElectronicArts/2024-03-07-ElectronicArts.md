---
layout: event
pos: 1.0
kurs: 123.42
skurs: 116.639
perf: 0.993
waehrung: EUR
trend: 0
risiko: 0
perspektive: langfristig
kommentar: Kurs prallte am letzten Hoch ab und könnte weiter konsolidieren. Position bleibt dennoch bestehen, da nach wie vor aussichtsreich.
---
