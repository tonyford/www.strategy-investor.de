from pytradegate import Instrument, Request
import json 
import sys 
import time 
import yfinance
import requests

Kurse = {}

user_agent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"
header = {'user-agent': user_agent}
request = Request(header=header)


with open('html/_site/aktien.json', 'r') as file:
    Aktien = json.load(file)

for a in Aktien:
    aktie = Aktien[a]
    if aktie['marketplace'] == 'tradegate':
        print( aktie['isin'] )
        K = Instrument(aktie['isin'], request)
        bid = str(K.bid).replace(' ', '').replace(',','.')
        ask = str(K.ask).replace(' ', '').replace(',','.')
        
        kurs = ( float(bid) + float(ask) ) / 2
        Kurse[aktie['isin']] = kurs
    else:
        print( aktie['ticker'] )
        Q = yfinance.Ticker(aktie['ticker'])
        daten = Q.fast_info
        Kurse[aktie['isin']] = daten.last_price

    time.sleep(5)

print('^GDAXI')
Q = yfinance.Ticker('^GDAXI')
daten = Q.fast_info
Kurse['DAX'] = daten.last_price

url = "https://www.strategy-investor.de/kursupdate.php"
headers = {'Content-type': 'application/json'}
print(requests.post(url, json={ 'p': 'PWD', 'd' : Kurse }, headers=headers))